{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Markov Decision Processes (MDP)\n",
    "\n",
    "MDP are a classical formalization of sequencial decision making problems where actions can effect future rewards. It's the reference framework to defining and working on RL problems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](img/rl_framework.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The reinforcement learning (RL) framework is characterized by an **agent** learning to interact with its **environment**.\n",
    "- At each time step, the agent receives the environment's **state** (the environment presents a situation to the agent), and the agent must choose an appropriate **action** in response. One time step later, the agent receives a **reward** (the environment indicates whether the agent has responded appropriately to the state) and a new **state**.\n",
    "- All agents have the goal to maximize expected **cumulative reward**, or the expected sum of rewards attained over all time steps.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Episodic vs. Continuing Tasks\n",
    "\n",
    "- A **task** is an instance of the reinforcement learning (RL) problem.\n",
    "- **Continuing tasks** are tasks that continue forever, without end.\n",
    "- **Episodic tasks** are tasks with a well-defined starting and ending point.\n",
    "    - In this case, we refer to a complete sequence of interaction, from start to finish, as an **episode**.\n",
    "    - Episodic tasks come to an end whenever the agent reaches a **terminal state**.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Reward Hypothesis\n",
    "\n",
    "- **Reward Hypothesis**: All goals can be framed as the maximization of (expected) cumulative reward."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cumulative Reward\n",
    "\n",
    "- The **return at time step** *t* is $G_t := R_{t+1} + R_{t+2} + R_{t+3} + \\ldots$\n",
    "- The agent selects actions with the goal of maximizing expected (discounted) return."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discounted Return\n",
    "\n",
    "- The **discounted return at time step** *t* is $G_t := R_{t+1} + \\gamma R_{t+2} + \\gamma^2 R_{t+3} + \\ldots$\n",
    "- The **discount rate** γ is something that you set, to refine the goal that you have the agent.\n",
    "    - It must satisfy 0≤γ≤1.\n",
    "    - If γ=0, the agent only cares about the most immediate reward.\n",
    "    - If γ=1, the return is not discounted.\n",
    "    - For larger values of γ, the agent cares more about the distant future. Smaller values of γ result in more extreme discounting, where - in the most extreme case - agent only cares about the most immediate reward.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## MDPs and One-Step Dynamics\n",
    "\n",
    "- The state space $\\mathcal{S}$ is the set of all (*nonterminal*) states.\n",
    "- In episodic tasks, we use $\\mathcal{S}^+$ to refer to the set of all states, including terminal states.\n",
    "- The **action space $\\mathcal{A}$** is the set of possible actions. (Alternatively, **$\\mathcal{A}(s)$** refers to the set of possible actions available in state $s \\in \\mathcal{S}$)\n",
    "- The **one-step dynamics** of the environment determine how the environment decides the state and reward at every time step. The dynamics can be defined by specifying $p(s',r|s,a) \\doteq \\mathbb{P}(S_{t+1}=s', R_{t+1}=r|S_{t} = s, A_{t}=a)$  for each possible $s', r, s, \\text{and } a$.\n",
    "- A **(finite) Markov Decision Process (MDP)** is defined by:\n",
    "    - a (finite) set of states $\\mathcal{S}$ (or $\\mathcal{S}^+$, in the case of an episodic task)\n",
    "    - a (finite) set of actions $\\mathcal{A}$\n",
    "    - a set of rewards $\\mathcal{R}$\n",
    "    - the one-step dynamics of the environment\n",
    "    - the discount rate $\\gamma \\in [0,1]$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](img/state_value_function.png)\n",
    "\n",
    "State-value function for golf-playing agent (Sutton and Barto, 2017)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Policies\n",
    "\n",
    "- A **deterministic policy** is a mapping $\\pi: \\mathcal{S}\\to\\mathcal{A}$. For each state $s\\in\\mathcal{S}$, it yields the action $a\\in\\mathcal{A}$ that the agent will choose while in state s.\n",
    "- A **stochastic policy** is a mapping $\\pi: \\mathcal{S}\\times\\mathcal{A}\\to [0,1]$. For each state $s\\in\\mathcal{S}$ and action $a\\in\\mathcal{A}$, it yields the probability $\\pi(a|s)$ that the agent chooses action $a$ while in state $s$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## State-Value Functions\n",
    "\n",
    "- The **state-value function** for a policy $\\pi$ is denoted $v_\\pi$. For each state $s \\in\\mathcal{S}$, it yields the expected return if the agent starts in state $s$ and then uses the policy to choose its actions for all time steps. That is, $v_\\pi(s) \\doteq \\text{} \\mathbb{E}_\\pi[G_t|S_t=s]$. We refer to $v_\\pi(s)$ as the **value of state $s$ under policy $\\pi$**.\n",
    "- The notation $\\mathbb{E}_\\pi[\\cdot]$ is borrowed from the suggested textbook, where $\\mathbb{E}_\\pi[\\cdot]$ is defined as the expected value of a random variable, given that the agent follows policy $\\pi$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bellman Equations\n",
    "\n",
    "- The **Bellman expectation equation for** $v_\\pi$ is: $v_\\pi(s) = \\text{} \\mathbb{E}_\\pi[R_{t+1} + \\gamma v_\\pi(S_{t+1})|S_t =s]$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimality\n",
    "\n",
    "- A policy $\\pi'$ is defined to be better than or equal to a policy $\\pi$ if and only if $v_{\\pi'}(s) \\geq v_\\pi(s)$ for all $s\\in\\mathcal{S}$.\n",
    "- An **optimal policy** $\\pi_*$ satisfies $\\pi_* \\geq \\pi$ for all policies $\\pi$. An optimal policy is guaranteed to exist but may not be unique.\n",
    "- All optimal policies have the same state-value function $v_*$, called the **optimal state-value function**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Action-Value Functions\n",
    "\n",
    "- The **action-value function** for a policy $\\pi$ is denoted $q_\\pi$. For each state $s \\in\\mathcal{S}$ and action $a \\in\\mathcal{A}$, it yields the expected return if the agent starts in state $s$, takes action $a$, and then follows the policy for all future time steps. That is, $q_\\pi(s,a) \\doteq \\mathbb{E}_\\pi[G_t|S_t=s, A_t=a]$. We refer to $q_\\pi(s,a)$ as the value of taking action aaa in state $s$ under a policy $\\pi$ (or alternatively as the **value of the state-action pair** $s$, $a$).\n",
    "- All optimal policies have the same action-value function $q_*$, called the **optimal action-value function**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimal Policies\n",
    "\n",
    "- Once the agent determines the optimal action-value function $q_*$, it can quickly obtain an optimal policy $\\pi_*$ by setting $\\pi_*(s) = \\arg\\max_{a\\in\\mathcal{A}(s)} q_*(s,a)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
