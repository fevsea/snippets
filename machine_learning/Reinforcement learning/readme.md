## Acknowledgments

Some content have been obtained from the Udacity nanodegree on machine learning.

## Other resources

https://indoml.com/2018/02/14/study-notes-reinforcement-learning-an-introduction/
https://github.com/FrancescoSaverioZuppichini/Reinforcement-Learning-Cheat-Sheet/blob/master/rl_cheatsheet.pdf