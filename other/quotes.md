Everythin changes and nothing stands still. Heraclitus of Ephesus

If the highest aim of a captain was the preserve his ship, he would keep it in port forever.
Stl Thomas Aquinas

The best time to plant a tree was twenty years ago. The second-best time is now.
Chinese proverb



Any time scientists disagree, it’s because we have insufficient data. Then we can agree on what kind of data to get; we get the data; and the data solves the problem. Either I’m right, or you’re right, or we’re both wrong. And we move on.
Neil deGrasse Tyson


# Tech

For a succeful technology, reality must take precedence over public relations, for nature cannot be fooled. Richard Feynman.

It is only when they go wrong that machines remind you how powerful they are.
Clive James

The major difference between a thing that might go wrong and a thing that cannot possibly go wrong is that when a thing that cannot possibly go wrong goes wrong it usually turns out to be impossible to get at or repair.
Douglas Adams

One machine can do the work of fifty ordinary men. No machine can do the work of one extraordinary man.
Elbert Hubbard 

Data don't give up their secrets easily. They must be tortured to confess.
Jeff Hooper

Computers are useless. They can only give you answers.
Pablo Picasso 

Once a new technology rolls over you, if you're not part of the steamroller, you're part of the road.
Stewart Brand

Is it better to be alive and wrong or right and dead?
Jay Kreps

# Games / animations

Science isn't about WHY, it's about WHY NOT!.
J.K. Simmons 

“When life gives you lemons, don’t make lemonade. Make life take the lemons back! Get mad! I don’t want your damn lemons, what the hell am I supposed to do with these? Demand to see life’s manager! Make life rue the day it thought it could give Cave Johnson lemons! Do you know who I am? I’m the man who’s gonna burn your house down! With the lemons! I’m gonna get my engineers to invent a combustible lemon that burns your house down!”
J.K. Simmons 

Life happens wherever you are, whether you make it or not.
Uncle Iroh

My story is a lot like yours, only more interesting ’cause it involves robots.
Bender
