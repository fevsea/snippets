// Using promisses
doSomething().then(successCallback, failureCallback);

// A proimise can be on a prnding, fullfiled or rejected state.

// Converting old style callbacks
const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
wait(10*1000).then(() => saySomething("10 seconds")).catch(failureCallback);

// Shortcut for chaining callbacks
Promise.resolve().then(func1).then(func2).then(func3);

// Custom promise
let myFirstPromise = new Promise((resolve, reject) => {
  setTimeout( function() {
    resolve("Success!")
  }, 250) 
})





// MODULES
// We need to say witch object we want to export
// We can do it at the declaration point
// Modules use strict mode automatically, and are deferred automatically.

export const name = 'square';

export function draw(ctx, length, x, y, color) {
    // pass
};

// Or as a statement at the end of the file
export { name, draw, reportArea, reportPerimeter };


// Importing features on js
import { name, draw, reportArea, reportPerimeter } from './modules/square.js';

// Declare js file to use module
// You can only use import and export statements inside modules; not regular scripts.
//<script type="module" src="main.js"></script>

// Using default -> Define a single export
export default function(ctx) {

};

import randomSquare from './modules/square.js';
import {default as randomSquare} from './modules/square.js';
    
// To avoid naming conflicts exports and imports can be renamed
    
export {
  function1 as newFunctionName,
  function2 as anotherNewFunctionName
};

import { function1 as newFunctionName,
         function2 as anotherNewFunctionName } from './modules/module.js';

// We can also import all the objects of a module into a Module Object:
import * as Module from './modules/module.js';
Module.function1() // ...

// To aggregate a modules hierarchy we can export inner modules
export * from 'x.js'
export { name } from 'x.js'