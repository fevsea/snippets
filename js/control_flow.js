if (condition_1) {
  statement_1;
} else if (condition_2) {
  statement_2;
} else if (condition_n) {
  statement_n;
} else {
  statement_last;
}


switch (expression) {
  case label_1:
    statements_1
    [break;]
  case label_2:
    statements_2
    [break;]
    ...
  default:
    statements_def
    [break;]
}

var a =  condición ? expr1 : expr2
// Lops and iterations

for ([initialExpression]; [condition]; [incrementExpression])
  statement

// Iterates a specified variable over all the enumerable properties of an object (but only user defined ones)
for (variable in object)  // Do not use in arrays
  statements

// Iterates over iterable objects (Arraus, Map, Set, arguments...)
for (variable of object)
  statement


do
  statement
while (condition);

while (condition)
  statement

labelName:
loopStructure(expression){
  break labelName;
  continue labelName;
}

inlineLabel: while (true) {
    break;
}



