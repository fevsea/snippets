// Case-sensitive with Unicode

// Variablres
// Global: declared outside function, accesyble anywhere in the file
// Local: declared inside function, accesible anywhere inside function
// BLock-scope: declared inside block, accesible only insithe that block

var x = 42;  // Global or local
let y = 13;  // BLock-scope Variablres

var a;  // a == undefined
// undefined eval as false but is converned to NaN in numerical context
// null eval as false but is converted to = in numerical context

const PI = 3.14;  // Same scope as let, must be initialized

/*
* Global variables are hoised to the top, but keep an undefined value until assigned.
* With functions only function declaration are hoisted, but not assignments
*/

// On a website the object window is the global object, so you can acces global fariables like window.variable

/*
* Constants var cannot change or be redeclared
*  but the properties of objects are not protected
*/


/*
*  Data types
*  Booleadn: true |  false
*  Null: null
*  Number: 42 | 3.1415
*  Undefined: undefined
*  String: "js"
*  Symbol
*/

// Literals

var coffees = ['French Roast', "Colombian", 'Kona'];
var fish = ["Lion", , 'Angel'];  // fish[1] == undefined
['home', , ,'school', ,].length == 5
var re = /ab+c/;  / Regex
// Better to us undefined instead of , ,

/*
* 0, 117 and -345 (decimal, base 10)
* 015, 0001 and -0o77 (octal, base 8)
* 0x1123, 0x00111 and -0xF1A7 (hexadecimal, "hex" or base 16)
* 0b11, 0b0011,  -0b11 (binary, base 2)
/*

/*
* 3.1415926
* -.123456789
* -3.1E+12
* .1e-23
*/

// Strings
// Basic literal string creation
`In JavaScript '\n' is a line-feed.\
with line break scaped`

// Multiline strings
`In JavaScript template strings can run
 over multiple lines, but double and single
 quoted strings cannot.`

// String interpolation
var name = 'Bob', time = 'today';
`Hello ${name}, how are you ${time}?`


// Conversions
x = 'The answer is ' + 42 // "The answer is 42"
y = 42 + ' is the answer' // "42 is the answer"
'37' - 7 // 30
'37' + 7 // "377"

// It's a good practice to indicate the base (second parameter)
parseInt()
parseFloat()

// objects
var car = { myCar: 'Saturn', getCar: carTypes('Honda'), special: sales };

var car = { manyCars: {a: 'Saab', 'b': 'Jeep'}, 7: 'Mazda' };
console.log(car.manyCars.b); // Jeep
console.log(car[7]); // Mazda

// evaluates to false; false undefined null 0 NaN ""

// Restructuring
var foo = ['one', 'two', 'three'];

// without destructuring
var one   = foo[0];
var two   = foo[1];
var three = foo[2];

// with destructuring
var [one, two, three] = foo;

// Normal operators try to convert types
3 == '3'  // true
3 === '3'  // false

propNameOrNumber in objectName  // return booloean
objectName instanceof objectType  // return boolean

// Spread operator
var parts = ['shoulder', 'knees'];
var lyrics = ['head', ...parts, 'and', 'toes'];

function f(x, y, z) { }
var args = [0, 1, 2];
f(...args);


// Operator recednce
member 	. []
call / create instance 	() new
negation/increment 	! ~ - + ++ -- typeof void delete
multiply/divide 	* / %
addition/subtraction 	+ -
bitwise shift 	<< >> >>>
relational 	< <= > >= in instanceof
equality 	== != === !==
bitwise-and 	&
bitwise-xor 	^
bitwise-or 	|
logical-and 	&&
logical-or 	||
conditional 	?:
assignment 	= += -= *= /= %= <<= >>= >>>= &= ^= |=
comma 	,


// Miscelanuous
delete objectName  // returns true or false. Does not work with var-declared elements
typeof var  // return string with type
typeof null;  // objectName
void statements  // Eveluates withour returnig anything


// Numbers

//Literals

+Infinity
-Infinity
NaN

0, 117 and -345 (decimal, base 10)
015, 0001 and -0o77 (octal, base 8)  // Add a leading zero -> Do not use leading zero
0x1123, 0x00111 and -0xF1A7 (hexadecimal, "hex" or base 16)
0b11, 0b0011,  -0b11 (binary, base 2)


3.1415926
-.123456789
-3.1E+12
.1e-23


// All in ieee double.-pprecisio 64 (no integers), (253 -1) and 253 -1) and + Infinity, - Infinity, NaN

// Number methods

Number.MAX_VALUE  // The largest representable number (±1.7976931348623157e+308)
Number.MIN_VALUE  //The smallest representable number (±5e-324)
Number.NaN  //Special "not a number" value
Number.NEGATIVE_INFINITY  // Special negative infinite value; returned on overflow
Number.POSITIVE_INFINITY  // Special positive infinite value; returned on overflow
Number.EPSILON  // Difference between 1 and the smallest value greater than 1 that can be represented as a Number (2.220446049250313e-16)
Number.MIN_SAFE_INTEGER  // Minimum safe integer in JavaScript (−253 + 1, or −9007199254740991)
Number.MAX_SAFE_INTEGER  // Maximum safe integer in JavaScript (+253 − 1, or +9007199254740991)

Number.parseFloat()  // Parses a string argument and returns a floating point number. Same as the global parseFloat() function.
Number.parseInt()  // Parses a string argument and returns an integer of the specified radix or base. Same as the global parseInt() function.
Number.isFinite()  // Determines whether the passed value is a finite number.
Number.isInteger()  // Determines whether the passed value is an integer.
Number.isNaN()  // Determines whether the passed value is NaN. More robust version of the original global isNaN().
Number.isSafeInteger()  // Determines whether the provided value is a number that is a safe integer.

toExponential()  // Returns a string representing the number in exponential notation.
toFixed()  // Returns a string representing the number in fixed-point notation.
toPrecision()  // Returns a string representing the number to a specified precision in fixed-point notation.

Math.
abs(), sin(), cos(), tan()
asin(), acos(), atan(), atan2()
sinh(), cosh(), tanh()
asinh(), acosh(), atanh()
pow(), exp(), expm1(), log10(), log1p(), log2()
floor(), ceil() 
min(), max()
random()
round(), fround(), trunc()
sqrt(), cbrt(), hypot()
sign()
clz32(), imul()

// Bitwise operators work at a 32bits (if greather, most significatn bits are discarded)


//   DATES   //

var dateObjectName = new Date([parameters]);
// Calling Date withouth new, returns a string representation
var today = new Date();
var endYear = new Date(1995, 11, 31, 23, 59, 59, 999); // Set day and month
endYear.setFullYear(today.getFullYear()); // Set year to this year
var msPerDay = 24 * 60 * 60 * 1000; // Number of milliseconds per day
var daysLeft = (endYear.getTime() - today.getTime()) / msPerDay;
var daysLeft = Math.round(daysLeft); //returns days left in the year

// Date internazionalization
const msPerDay = 24 * 60 * 60 * 1000;
const july172014 = new Date(msPerDay * (44 * 365 + 11 + 197));
const options = { year: '2-digit', month: '2-digit', day: '2-digit',
                hour: '2-digit', minute: '2-digit', timeZoneName: 'short' };
const americanDateTime = new Intl.DateTimeFormat('en-US', options).format;
console.log(americanDateTime(july172014)); // 07/16/14, 5:00 PM PDT



//  STRINGS    //

// Altought JS has autobxing is better to use litterals to avaid unexected situations

const firstString = '2 + 2'; // Creates a string literal value
const secondString = new String('2 + 2'); // Creates a String object
eval(firstString); // Returns the number 4
eval(secondString); // Returns the string "2 + 2"

// Internazionalization

const gasPrice = new Intl.NumberFormat('en-US',
                        { style: 'currency', currency: 'USD',
                          minimumFractionDigits: 3 });
console.log(gasPrice.format(5.259)); // $5.259

// Sorting strings 
function letterSort(lang, letters) {
  letters.sort(new Intl.Collator(lang).compare);
  return letters;
}


// Methods

charAt, charCodeAt, codePointAt  // Return the character or character code at the specified position in string.
indexOf, lastIndexOf  // Return the position of specified substring in the string or last position of specified substring, respectively.
startsWith, endsWith, includes  // Returns whether or not the string starts, ends or contains a specified string.
concat  // Combines the text of two strings and returns a new string.
split  // Splits a String object into an array of strings by separating the string into substrings.
slice  // Extracts a section of a string and returns a new string.
substring, substr  // Return the specified subset of the string, either by specifying the start and end indexes or the start index and a length.
match, matchAll, replace, search  // Work with regular expressions.
toLowerCase, toUpperCase  // Return the string in all lowercase or all uppercase, respectively.
normalize  // Returns the Unicode Normalization Form of the calling string value.
repeat  // Returns a string consisting of the elements of the object repeated the given times.
trim  // Trims whitespace from the beginning and end of the string.


// Backticks strings can spand multiple lines and accept interpolation in the form of ${}
`multiline
string with ${interpolation_var} on string`


// Nullish coalescing operator (??) is returns its right-hand side operand when its left-hand side operand is null or undefined. Util for default values

// The comma operator (,) simply evaluates both of its operands and returns the value of the last operand. Util for multiple statements on a for loop initialization.
