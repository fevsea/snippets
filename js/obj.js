var myCar = new Object();
myCar.make = 'Ford';
myCar.color; // undefined

var myObj = new Object(),
    obj = 'make';

myObj.type = 'Dot syntax';
myObj[obj] = 'String with space';  // Set obf.toString() is as a key

for (var i in obj) {
  var result = '';
  // obj.hasOwnProperty() is used to filter out properties from the object's prototype chain
  if (obj.hasOwnProperty(i)) {
    result += objName + '.' + i + ' = ' + obj[i] + '\n';
  }
}

Object.keys(obj); // returns an array with all the own names (only enumerable)
Object.getOwnPropertyNames(obj) // return an array with all own names

// Object inizializers
var myHonda = {color: 'red', wheels: 4, engine: {cylinders: 4, size: 2.2}};


// Object constructor. Name with capital letter
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}
var car1 = new Car('Eagle', 'Talon TSi', 1993);


// Creating objects using Object.create
var Animal = {
  type: 'Invertebrates', // Default value of properties
  displayType: function() {  // Method which will display type of Animal
    console.log(this.type);
  }
};
var animal1 = Object.create(Animal);
// Methods work like properties. When a method refers to this, refers to the outher object


var d = Date.prototype;
Object.defineProperty(d, 'year', {
  get: function() { return this.getFullYear(); },
  set: function(y) { this.setFullYear(y); }
});

// Getters and setters
var o = {
  a: 5,
  get b() {
    return this.a + 1;
  },
  set b(x) {
    this.a = x / 2;
  }
};
b


// Ecmna2015 classes

use strict';

class Polygon {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}

class Square extends Polygon {
  constructor(sideLength) {
    super(sideLength, sideLength);
  }
  get area() {
    return this.height * this.width;
  }
  set sideLength(newLength) {
    this.height = newLength;
    this.width = newLength;
  }
}

var square = new Square(2);


// Inheritance

function Engineer(name, projs, mach) {
  WorkerBee.call(this, name, 'engineering', projs);
  this.machine = mach || '';
}

function Engineer(name, projs, mach) {
  this.base = WorkerBee;
  this.base(name, 'engineering', projs);
  this.machine = mach || '';
}



// ITERATOR
// An iterator is an object that implements the function `next()` thet returns an object {value, done}

// Manual declaration of custom iterator
function makeRangeIterator(start = 0, end = Infinity, step = 1) {
    let nextIndex = start;
    let iterationCount = 0;

    const rangeIterator = {
       next: function() {
           let result;
           if (nextIndex < end) {
               result = { value: nextIndex, done: false }
               nextIndex += step;
               iterationCount++;
               return result;
           }
           return { value: iterationCount, done: true }
       }
    };
    return rangeIterator;
}

// Generator functions
// This retrun an iterable object which can yield values
function* makeRangeIterator(start = 0, end = 100, step = 1) {
    let iterationCount = 0;
    for (let i = start; i < end; i += step) {
        iterationCount++;
        yield i;
    }
    return iterationCount;
}

// In order to be iterable, an object must implement the @@iterator method

const myIterable = {
    *[Symbol.iterator]() {
        yield 1;
        yield 2;
        yield 3;
    }
}

// The next() function can receive an argument, which will be sated to the yield statemetn
let reset = yield current;