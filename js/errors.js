try {
  throw new Error('oops');
} catch (ex) {
  console.error('inner', ex.message);
} finally {
  console.log('finally');
}

// Finally is always executed even if a return statement is executed on try or catch. If a retruen statement also exist on a finally clausule it will override the previous return or throw

// Any object can be trown but is usually better to use standar errors
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#Error_types

EvalError
RangeError
ReferenceError
SyntaxError
TypeError
URIError

// An standar error has two properties;
Error.prototype.message  // Error message
Error.prototype.name  // Error name



// Create an object type UserException
function UserException(message) {
  this.message = message;
  this.name = 'UserException';
}

// Make the exception convert to a pretty string when used as a string 
// (e.g., by the error console)
UserException.prototype.toString = function() {
  return `${this.name}: "${this.message}"`;
}

// Create an instance of the object type and throw it
throw new UserException('Value too high');