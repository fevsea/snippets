sudo apt update
sudo apt -y upgrade

sudo addgroup software
sudo adduser ubuntu software

sudo chgrp -R software /opt
sudo chmod g+ws -R /opt

# Install conde
# https://conda.io/docs/user-guide/install/linux.html
# wget xxxxxxxx
# bash xxxxxxxxxx.sh

# Add to bashrc
# export PATH="/opt/anaconda3/bin:$PATH"
source ~/.bashrc

conda update -n base conda
conda create --name jupyter

conda install -c conda-forge jupyterlab
conda install -c conda-forge jupyterhub
conda install notebook
conda install -c conda-forge nodejs
jupyter labextension install @jupyterlab/hub-extension
jupyter labextension install @jupyterlab/git
conda install -c conda-forge matplotlib scipy pandas numpy seaborn
conda update --all


sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot 

sudo certbot certonly --standalone -d agustin.xyz


sudo add-apt-repository ppa:chronitis/jupyter
sudo apt-get update
sudo apt-get install ihaskell ijulia ijavascript irkernel

sudo apt-get install texlive texlive-latex-extra pandoc


# Scala
SCALA_VERSION=2.12.10 ALMOND_VERSION=0.9.1
curl -Lo coursier https://git.io/coursier-cli
chmod +x coursier
./coursier bootstrap \
    -r jitpack \
    -i user -I user:sh.almond:scala-kernel-api_$SCALA_VERSION:$ALMOND_VERSION \
    sh.almond:scala-kernel_$SCALA_VERSION:$ALMOND_VERSION \
    -o almond
./almond --install