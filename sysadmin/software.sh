echo "Updating package list"
sudo apt update

echo "Upgrading packeges"
sudo apt upgrade -y

fi
#Firefox for developers
echo "Installing Firefox for developers"
sudo add-apt-repository -y ppa:mozillateam/firefox-next
sudo apt update
sudo apt-get -y install firefox

# TLP battery saving
#sudo add-apt-repository -y ppa:linrunner/tlp
#sudo apt-get install -y tlp tlp-rdw
#Only thinkpad
#sudo apt-get install -y tp-smapi-dkms acpi-call-tools
#sudo tlp start

#VLC
echo "Installing VLC"
sudo apt install -y vlc browser-plugin-vlc libavcodec-extra
#Blender
echo "Installing Blender"
sudo apt install -y blender
#Virtualbox
echo "Installing Virtualbox"
sudo apt install -y virtualbox
#Mocp
echo "Installing Mocp"
sudo apt install -y moc

# OpenSCAD
sudo add-apt-repository -y  ppa:openscad/releases
sudo apt-get update
sudo apt-get install -y openscad

#gparted
echo "Installing Gparted"
sudo apt install -y gparted

echo "Installing python"
sudo apt install -y python3 python3-pip ipython3

echo "Installing dev tools"
sudo apt install -y  git lm-sensors openssh-server openssh-client tmux hardinfo curl wireshark-qt aircrack-ng nmap

echo "Sysadmin tools"
sudo apt install -y iotop htop

echo "Instaling ides"
#Sublime atom
echo "Installing atom"
curl -L https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
sudo apt-get update
sudo apt-get install -y atom
sudo snap install pycharm-professional --classic

echo "Installing random stuff"
sudo apt install -y sl cowsay cmatrix lolcat powertop
sudo pip3 install thefuck
echo "eval $(thefuck --alias)" >> ~/.bashrc
source ~/.bashrc

echo "Python stuff"
sudo pip3 install numpy scipy pandas matplotlib scikit-learn tqdm ipython jupyter gym seaborn


echo "R lang"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
sudo apt update
sudo apt install r-base

######################

#UPCNet
#sudo apt update
#sudo apt-get install -y libc6-i386 lib32z1
# https://serveistic.upc.edu/ca/upclink/documentacio/manual-ubuntu

#alias wake="sudo etherwake -i enp3s0 00:21:cc:b7:dd:14"
#sudo dpkg -i ps-pulse-linux-8*-ubuntu-debian-installer.deb
#sudo apt-get install -y libwebkitgtk-1.0-0:i386
#sudo apt-get install -y libproxy1-plugin-gsettings:i386
#sudo apt-get install -y libproxy1-plugin-webkit:i386
#sudo apt-get install -y libdconf1:i386
#sudo apt-get install -y dconf-gsettings-backend:i386
#https://vpn.upc.edu/estudiants

# Docker

sudo apt-get remove docker docker-engine docker.io
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.22.0/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose



#  Restricted packages
sudo apt install ubuntu-restricted-extras
sudo apt install flashplugin-installer
sudo apt-get install libcanberra-gtk-module:i386
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module

# Workd grid
sudo apt-get install chrome-gnome-shell gnome-tweak-tool


# Fingerprint integratin
sudo add-apt-repository ppa:fingerprint/fprint
sudo apt-get install libfprint0 fprint-demo libpam-fprintd
sudo pam-auth-update

# X1 carbon gen 3 touchpad fix
#https://askubuntu.com/questions/970988/two-finger-scrolling-not-working-since-ubuntu-17-10