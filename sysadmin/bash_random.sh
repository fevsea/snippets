# Prevent apache2 to be stasrted automatically
sudo update-rc.d apache2 disable   #enable

#Stress gpu (nvidia)
__GL_SYNC_TO_VBLANK=0 glxgears


# Tmux as default -> .bashrc
#make sure that (1) tmux exists on the system, (2) we're in an interactive shell, and (3) tmux doesn't ry to run within itself
if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
  exec tmux
fi

# Config tmux
#Copy .tmux.conf to home