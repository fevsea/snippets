# UML diagrams

![UML family](img/uml_family.png)

A diagram is usually enclosed by a rectangle with a pentagon in the top left-hand corner. This pentagon contains the diagram type and the
name of the diagram (CS - class diagram, SD - sequence diagram):

![UML family](img/pentagon_sample.png)

## Structure diagrams

Structure diagrams emphasize the things that must be present in the system being modeled.

### Class diagram
Describes the types of objects in the system and various kinds of static relationships which exist between them. 
![Class diagram](img/class_diagram.png)

### Object diagram
A static object diagram is an instance of a class diagram; it shows a snapshot of the detailed state of a system at a point in time.
![Object diagram](img/object_diagram.png)

### Package diagram
Structure diagram which shows packages and dependencies between the packages. Model diagrams allow to show different views of a system, 
![Package diagram](img/package_diagram.png)

### Component diagram
Depicts how components are wired together to form larger components or software systems. It illustrates the architectures of the software components and the dependencies between them.
![Component diagram](img/component_diagram.png)

### Composition structure diagram
It is a type of static structure diagram that shows the internal structure of a class and the collaborations that this structure makes possible. 
![Composition structure diagram](img/composition_diagram.png)

### Deployment diagram
It is a structure diagram which shows architecture of the system as deployment (distribution) of software artifacts to deployment targets.
![Deployment diagram](img/deployment_diagram.png)

### Profile diagram
Enables you to create domain and platform specific stereotypes and define the relationships between them.
![Profile diagram](img/profile_diagram.png)

## Behavior diagrams

Behavior diagrams emphasize what must happen in the system being modeled. 

### Use case diagram
It is a model of the system's intended functionality (use cases) and its environment (actors).
![Use case diagram](img/use_case_diagram.png)

### State machine diagram
State diagrams depict the permitted states and transitions as well as the events that effect these transitions.
![State machine diagram](img/state_machine_diagram.png)

### Activity diagram
Graphical representations of workflows of stepwise activities and actions with support for choice, iteration and concurrency
![Activity diagram](img/activity_diagram.png)

### Sequence diagram
It shows how the objects interact with others in a particular scenario of a use case.
![Sequence diagram](img/sequence_diagram.png)

### Communication diagram
Similar to Sequence Diagram, the Communication Diagram is also used to model the dynamic behavior of the use case. When compare to Sequence Diagram, the Communication Diagram is more focused on showing the collaboration of objects rather than the time sequence.
![Communication diagram](img/communication_diagram.png)

### Timing diagram
Timing Diagram shows the behavior of the object(s) in a given period of time.
![Timing diagram](img/timing_diagram.png)

### Interaction overview diagram
The Interaction Overview Diagram focuses on the overview of the flow of control of the interactions. It is a variant of the Activity Diagram where the nodes are the interactions or interaction occurrences. 
![Interaction overview diagram](img/interaction_overview_diagram.png)

# Other
`Abstract` is the opposite of `concrete`.